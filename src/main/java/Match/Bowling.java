package Match;

public class Bowling {
    private final BowlingType type;
    private final RandomRunGenerator randomRunGenerator;
    public int runGiven;

    public Bowling(BowlingType bowlerType, RandomRunGenerator randomRunGenerator) {
        this.type = bowlerType;
        this.randomRunGenerator = randomRunGenerator;
    }

    public BowlingType getType() {
        return type;
    }

    public void setRunGiven(int runGiven) {
        this.runGiven = runGiven;
    }

    public int generateRuns() {
        runGiven = randomRunGenerator.getRandomRun(7);
        return runGiven;
    }
}

