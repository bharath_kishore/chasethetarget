package Match;

public class Batting {
    private final RandomRunGenerator randomRunGenerator;
    public int runScored;
    public BattingType type;

    public Batting(BattingType batsmanType, RandomRunGenerator randomRunGenerator) {
        this.type = batsmanType;
        this.randomRunGenerator = randomRunGenerator;
    }

    public int generateRuns() {
        runScored = randomRunGenerator.getRun(type);
        return runScored;
    }
}