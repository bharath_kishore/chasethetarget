package Match;

import java.util.Random;

public class RandomRunGenerator {
    public int getRun(BattingType type) {
        int[] runs = type.bound;
        int index = getRandomRun(runs.length);
        return runs[index];
    }

    public int getRandomRun(int bound) {
        Random random = new Random();
        return random.nextInt(bound);
    }
}
