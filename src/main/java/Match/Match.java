package Match;

import java.util.Scanner;

public class Match {
    private static int over;

    private static final String PLAYER_ONE_WON = "PlayerOne has won";
    private static final String PLAYER_TWO_WON = "PlayerTwo has won";
    public static final int BALLS_PER_OVER = 6;
    public static int target = 0;
    Player player;

    public Match(int over) {
        this.over = over;
    }

    public int start(int innings) {
        Scanner input = new Scanner(System.in);
        RandomRunGenerator randomNumberGenerator = new RandomRunGenerator();
        System.out.println("player : Batting method: \n 0.Normal batsman \n 1.Hitter \n 2.Defensive \n 3.TailEnder");
        int batsmanType = input.nextInt();
        Batting batting = new Batting(BattingType.values()[batsmanType], randomNumberGenerator);
        System.out.println("player : Bowling method: \n 0.Normal bowler \n 1.Part time bowler ");
        int bowlerType = input.nextInt();
        Bowling bowling = new Bowling(BowlingType.values()[bowlerType], randomNumberGenerator);
        player = new Player(batting, bowling);
        return player.play(innings, over, target);
    }

    public String result(int target, int totalScore) {
        if (player.hasChased(totalScore, target)) {
            return PLAYER_TWO_WON;
        }
        return PLAYER_ONE_WON;
    }
}
