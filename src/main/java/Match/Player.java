package Match;

public class Player {
    private final Batting batting;
    private final Bowling bowling;

    public Player(Batting batting, Bowling bowling) {
        this.batting = batting;
        this.bowling = bowling;
    }

    public int play(int innings, int over, int target) {
        int totalScore = 0;
        for (int ball = 0; ball < over * Match.BALLS_PER_OVER; ball++) {
            System.out.println("Batting: " + batting.generateRuns());
            System.out.println("Bowling: " + bowling.generateRuns());
            System.out.println();
            boolean wicket = hasTakenAWicket(batting.runScored, bowling.runGiven, batting.type);
            if ((wicket) && bowling.getType() == BowlingType.PART_TIME_BOWLER) {
                continue;
            }
            if (wicket) {
                break;
            }
            totalScore += batting.runScored;
            if (innings == 2 && hasChased(totalScore, target)) {
                break;
            }
        }
        return totalScore;

    }

    public boolean hasChased(int totalScore, int target) {
        return totalScore >= target;
    }

    public boolean hasTakenAWicket(int runsScored, int runsGiven, BattingType battingType) {
        if (battingType == BattingType.TAIL_ENDER) {
            return (runsScored % 2 == 0 && runsGiven % 2 == 0) || (runsScored % 2 != 0 && runsGiven % 2 != 0);
        }
        return runsScored == runsGiven;
    }

}
