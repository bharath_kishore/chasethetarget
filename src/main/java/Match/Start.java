package Match;

import java.util.Scanner;

public class Start {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the No.of overs: ");
        int over = input.nextInt();
        Match match = new Match(over);
        int innings = 1;
        int target = match.start(innings);
        match.target = target;
        System.out.println("target :" + target);
        System.out.println(match.result(target, match.start(++innings)));
    }
}
