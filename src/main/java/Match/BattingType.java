package Match;

public enum BattingType {
    NORMAL_BATSMAN(new int[]{0, 1, 2, 3, 4, 5, 6}),
    HITTER(new int[]{0, 4, 6}),
    DEFENSIVE(new int[]{0, 1, 2, 3}),
    TAIL_ENDER(new int[]{0, 1, 2, 3, 4, 5, 6});

    public final int[] bound;

    BattingType(int[] bound) {
        this.bound = bound;
    }
}
