import Match.*;
import fakes.MockBatsman;
import fakes.MockBowler;
import fakes.MockRandomRunGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MatchTest {
    @Test
    void shouldReturnBatsmanHasWonMessageWhenTheRunsScoredByTheBatsmanReachedTheTarget() {
        int target = 4;
        int over = 1;
        MockBatsman mockBatsman=new MockBatsman(BattingType.NORMAL_BATSMAN);
        MockBowler mockBowler=new MockBowler(BowlingType.NORMAL_BOWLER);
        int batsmanRun=4;
        int bowlerRun=1;
        MockRandomRunGenerator mockRandomRunGenerator=new MockRandomRunGenerator(batsmanRun,bowlerRun);
        Match match = new Match(over, target, mockBatsman, mockBowler,mockRandomRunGenerator);

        match.play();

        assertEquals(1,mockRandomRunGenerator.getRandomRunCall);
        assertEquals(1,mockRandomRunGenerator.getRunCall);
        assertEquals(1,mockBowler.hasTakenWicketCall);
        assertEquals(1,mockBatsman.addScoreCall);
        assertEquals(1,mockBatsman.hasChasedCall);
        assertEquals(Match.BATSMAN_WON, match.getResult());
    }

    @Test
    void shouldReturnBatsmanHasLostMessageWhenTheRunsScoredByTheBatsmanNotReachedTheTarget() {
        int target = 12;
        int over = 1;
        int batsmanRun=1;
        int bowlerRun=0;
        MockBatsman mockBatsman=new MockBatsman(BattingType.NORMAL_BATSMAN);
        MockBowler mockBowler=new MockBowler(BowlingType.NORMAL_BOWLER);
        MockRandomRunGenerator mockRandomRunGenerator=new MockRandomRunGenerator(batsmanRun,bowlerRun);
        Match match = new Match(over, target, mockBatsman, mockBowler,mockRandomRunGenerator);

        match.play();

        assertEquals(6,mockRandomRunGenerator.getRandomRunCall);
        assertEquals(6,mockRandomRunGenerator.getRunCall);
        assertEquals(6,mockBowler.hasTakenWicketCall);
        assertEquals(6,mockBatsman.addScoreCall);
        assertEquals(6,mockBatsman.hasChasedCall);
        assertEquals(Match.BATSMAN_LOST, match.getResult());
    }

    @Test
    void shouldReturnBatsmanHasLostMessageWhenTheWicketIsLostBeforeChasingTheTarget() {
        int target = 4;
        int over = 1;
        int batsmanRun=0;
        int bowlerRun=2;
        MockBatsman mockBatsman=new MockBatsman(BattingType.NORMAL_BATSMAN);
        MockBowler mockBowler=new MockBowler(BowlingType.NORMAL_BOWLER);
        MockRandomRunGenerator mockRandomRunGenerator=new MockRandomRunGenerator(batsmanRun,bowlerRun);
        Match match = new Match(over, target, mockBatsman, mockBowler,mockRandomRunGenerator);

        match.play();

        assertEquals(6,mockRandomRunGenerator.getRandomRunCall);
        assertEquals(6,mockRandomRunGenerator.getRunCall);
        assertEquals(6,mockBowler.hasTakenWicketCall);
        assertEquals(6,mockBatsman.addScoreCall);
        assertEquals(6,mockBatsman.hasChasedCall);
        assertEquals(Match.BATSMAN_LOST, match.getResult());
    }
    @Test
    void shouldReturnBatsmanHasWonWhenTheRunsScoredByTheTailEnderBatsmanReachedTheTargetIfNormalBowlerBowled() {
        int target = 10;
        int over = 1;

        MockBatsman mockBatsman=new MockBatsman(BattingType.TAIL_ENDER);
        MockBowler mockBowler=new MockBowler(BowlingType.NORMAL_BOWLER);
        int batsmanRun=2;
        int bowlerRun=1;
        MockRandomRunGenerator mockRandomRunGenerator=new MockRandomRunGenerator(batsmanRun,bowlerRun);
        Match match = new Match(over, target, mockBatsman, mockBowler,mockRandomRunGenerator);

        match.play();

        assertEquals(5,mockRandomRunGenerator.getRandomRunCall);
        assertEquals(5,mockRandomRunGenerator.getRunCall);
        assertEquals(5,mockBowler.hasTakenWicketCall);
        assertEquals(5,mockBatsman.addScoreCall);
        assertEquals(5,mockBatsman.hasChasedCall);
        assertEquals(Match.BATSMAN_WON, match.getResult());
    }
    @Test
    void shouldReturnBatsmanHasLostWhenTheRunsScoredByTheTailEnderBatsmanReachedTheTargetIfNormalBowlerBowled() {
        int target = 10;
        int over = 1;
        int batsmanRun=2;
        int bowlerRun=2;
        MockBatsman mockBatsman=new MockBatsman(BattingType.TAIL_ENDER);
        MockBowler mockBowler=new MockBowler(BowlingType.NORMAL_BOWLER);
        MockRandomRunGenerator mockRandomRunGenerator=new MockRandomRunGenerator(batsmanRun,bowlerRun);
        Match match = new Match(over, target, mockBatsman, mockBowler,mockRandomRunGenerator);

        match.play();

        assertEquals(1,mockRandomRunGenerator.getRandomRunCall);
        assertEquals(1,mockRandomRunGenerator.getRunCall);
        assertEquals(1,mockBowler.hasTakenWicketCall);
        assertEquals(0,mockBatsman.addScoreCall);
        assertEquals(1,mockBatsman.hasChasedCall);
        assertEquals(Match.BATSMAN_LOST, match.getResult());
    }
    @Test
    void shouldReturnBatsmanHasWonWhenTheRunsScoredByTheTailEnderBatsmanNotReachedTheTargetIfPartTimeBowlerBowled() {
        int target = 12;
        int over = 1;
        int batsmanRun=2;
        int bowlerRun=1;
        MockBatsman mockBatsman=new MockBatsman(BattingType.TAIL_ENDER);
        MockBowler mockBowler=new MockBowler(BowlingType.PART_TIME_BOWLER);
        MockRandomRunGenerator mockRandomRunGenerator=new MockRandomRunGenerator(batsmanRun,bowlerRun);
        Match match = new Match(over, target, mockBatsman, mockBowler,mockRandomRunGenerator);

        match.play();

        assertEquals(6,mockRandomRunGenerator.getRandomRunCall);
        assertEquals(6,mockRandomRunGenerator.getRunCall);
        assertEquals(6,mockBowler.hasTakenWicketCall);
        assertEquals(6,mockBatsman.addScoreCall);
        assertEquals(6,mockBatsman.hasChasedCall);
        assertEquals(Match.BATSMAN_WON, match.getResult());
    }
    @Test
    void shouldReturnBatsmanHasLostWhenTheRunsScoredByTheTailEnderBatsmanNotReachedTheTargetIfPartTimeBowlerBowled() {
        int target = 12;
        int over = 1;
        int batsmanRun=1;
        int bowlerRun=0;
        MockBatsman mockBatsman=new MockBatsman(BattingType.TAIL_ENDER);
        MockBowler mockBowler=new MockBowler(BowlingType.PART_TIME_BOWLER);
        MockRandomRunGenerator mockRandomRunGenerator=new MockRandomRunGenerator(batsmanRun,bowlerRun);
        Match match = new Match(over, target, mockBatsman, mockBowler,mockRandomRunGenerator);

        match.play();

        assertEquals(6,mockRandomRunGenerator.getRandomRunCall);
        assertEquals(6,mockRandomRunGenerator.getRunCall);
        assertEquals(6,mockBowler.hasTakenWicketCall);
        assertEquals(6,mockBatsman.addScoreCall);
        assertEquals(6,mockBatsman.hasChasedCall);
        assertEquals(Match.BATSMAN_LOST, match.getResult());
    }
}

