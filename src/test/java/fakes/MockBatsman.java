package fakes;

import Match.Batting;
import Match.BattingType;

public class MockBatsman extends Batting {

    public int addScoreCall;
    public int hasChasedCall;

    public MockBatsman(BattingType type) {
        super(type);
    }

    @Override
    public void setRunScored(int runScored) {
        super.setRunScored(runScored);
    }

    @Override
    public int addScore() {
        addScoreCall++;
        return super.addScore();
    }

    @Override
    public boolean hasChased(int target) {
        hasChasedCall++;
        return super.hasChased(target);
    }
}
