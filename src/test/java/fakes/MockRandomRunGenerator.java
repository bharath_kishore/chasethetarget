package fakes;

import Match.BattingType;
import Match.RandomRunGenerator;

public class MockRandomRunGenerator extends RandomRunGenerator {
int batsmanRun;
int bowlerRun;
public int getRunCall;
public int getRandomRunCall;
    public MockRandomRunGenerator(int batsmanRun,int bowlerRun) {
        this.batsmanRun=batsmanRun;
        this.bowlerRun=bowlerRun;
    }

    @Override
    public int getRun(BattingType batsmanType) {
        getRunCall++;
        return batsmanRun;
    }

    @Override
    public int getRandomRun(int bound) {
        getRandomRunCall++;
        return bowlerRun;
    }
}
