package fakes;

import Match.BattingType;
import Match.Bowling;
import Match.BowlingType;

public class MockBowler extends Bowling {
  public  int hasTakenWicketCall;

    public MockBowler(BowlingType bowlerType) {
        super(bowlerType);
    }

    @Override
    public void setRunGiven(int runGiven) {
        super.setRunGiven(runGiven);
    }

    @Override
    public boolean hasTakenAWicket(int runs, BattingType batsmanType) {
        hasTakenWicketCall++;
        return super.hasTakenAWicket(runs,batsmanType);
    }
}
