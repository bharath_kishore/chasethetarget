import Match.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BatsmanTest {
    @Test
    void shouldReturnTrueWhenTheBatsmanChasedTheTarget() {
        int target = 3;
        int batsmanRun=4;
        Batting batsman = new Batting(BattingType.NORMAL_BATSMAN);
        batsman.setRunScored(batsmanRun);
        batsman.addScore();

        assertTrue(batsman.hasChased( target));
    }

    @Test
    void shouldReturnFalseWhenTheBatsmanNotChasedTheTarget() {
        int target = 12;
        int batsmanRun=6;
        Batting batsman = new Batting(BattingType.NORMAL_BATSMAN);
        batsman.setRunScored(batsmanRun);
        batsman.addScore();

        assertFalse(batsman.hasChased( target));
    }
}