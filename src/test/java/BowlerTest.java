import Match.BattingType;
import Match.Bowling;
import Match.BowlingType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class BowlerTest {

    @Test
    void shouldReturnTrueWhenTheBowlerHasTakenAWicket() {
        int batsmanRun=1;
        int bowlerRun=1;
        Bowling bowler =new Bowling(BowlingType.NORMAL_BOWLER);
        bowler.setRunGiven(bowlerRun);

        assertTrue(bowler.hasTakenAWicket(batsmanRun, BattingType.NORMAL_BATSMAN));
    }

    @Test
    void shouldReturnFalseWhenTheBowlerHasNotTakenAWicket() {
        int batsmanRun=2;
        int bowlerRun=1;
        Bowling bowler=new Bowling(BowlingType.NORMAL_BOWLER);
        bowler.setRunGiven(bowlerRun);

        assertFalse(bowler.hasTakenAWicket(batsmanRun, BattingType.NORMAL_BATSMAN));
    }

}